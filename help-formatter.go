package cmdhelper

import (
	"io"
	"regexp"
	"strings"
	"unicode"

	"github.com/fatih/color"
	"golang.org/x/net/html"
)

var tags = map[string]func(string) string{
	"p":       div,
	"pre":     pre,
	"black":   clr(color.FgBlack),
	"white":   clr(color.FgWhite),
	"red":     clr(color.FgRed),
	"green":   clr(color.FgGreen),
	"blue":    clr(color.FgBlue),
	"cyan":    clr(color.FgCyan),
	"magenta": clr(color.FgMagenta),
	"yellow":  clr(color.FgYellow),
	"default": div,
}

func pre(str string) string {
	return strings.TrimRightFunc(str, unicode.IsSpace)
}
func div(str string) string {
	// str = strings.TrimSpace(str)
	str = regexp.MustCompile(`[ \t\n\r]+`).ReplaceAllString(str, " ")
	str = breakLines(str, 80)
	if str == "" {
		return ""
	}
	return str
}

func clr(value ...color.Attribute) func(string) string {
	return func(str string) string {
		return color.New(value...).Sprint(str)
	}
}

func MustFormatString(str string) string {
	s, err := FormatString(str)
	if err != nil {
		panic(err)
	}
	return s
}
func FormatString(str string) (string, error) {
	return Format(strings.NewReader(str))
}
func Format(r io.Reader) (string, error) {
	doc, err := html.Parse(r)
	if err != nil {
		return "", err
	}
	o := FormatNode(doc)
	o = strings.TrimSpace(o)
	return o, nil
}

func FormatNode(node *html.Node) string {
	// fmt.Printf("%s\n", node.Data)
	output := ""
	current := node.FirstChild
	for current != nil {
		switch current.Type {
		case html.TextNode:
			fn, ok := tags[current.Parent.Data]
			if !ok {
				fn = tags["default"]
			}
			output += fn(current.Data)
		case html.DocumentNode, html.ElementNode:
			output += FormatNode(current)
		default:
		}
		current = current.NextSibling
	}

	if node.Data == "p" || node.Data == "pre" {
		return "\n" + output + "\n"
	}

	return output
}

func breakLines(str string, length int) string {
	out := ""
	lines := strings.Split(str, "\n")
	for _, line := range lines {
		words := strings.Split(line, " ")
		newLine := ""
		for _, word := range words {
			if len(newLine)+len(word) > length {
				out += newLine + "\n"
				newLine = ""
			}
			newLine += word + " "
		}
		out += newLine[:len(newLine)-1]
	}
	return out
}
